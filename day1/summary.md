# markdown学习

## 标题

一级标题
========
# 一级标题

二级标题
----------
## 二级标题


### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题




## 段落
通过2个空格的换行和通过一个单独空行的换行，行与行的间距不一样大

这个一个2行的段落  
这个段落的第二行


这个一个2行的段落

这个段落的第二行

*斜体*  
__粗体__

下面是分割线 三个以上 * - _ 中间可以有空格
***
* * *
---
- - -
___
_ _ _

~~删除线~~  
<u>下划线</u>  


脚注没有成功，原因待查看  
[^要加提示的文本]  
[^RUNOOB]  
[^RUNOOB ]: 要显示的文本内容

# git fork

前提  
已forkA项目到远程操作B 并将远程B下载到本地C  
A项目有更新，如何再保存本地修改的情况下，同步A的修改  
1. git remote add upstream https://gitee.com/zhufengpeixun/zhufeng-vue-project.git  
2. git fetch upstream
3. git stash -a
4. git merge upstream/master
5. git stash pop  
为啥我不直接点远程仓库的同步按钮，直接同步A和B呢，非要CmergeA呢。鉴于时间关系，这部分待研究

再没有任何push之前可以
直接点击项目B项目上的刷新按钮同步，后面需要CmergeA，然后pushB吧。。。
参考链接：https://www.jianshu.com/p/d73dcee2d907

# 环境搭建
按图片一步步操作没有遇到问题，可视化很方便

# 路由系统配置
写index.router.js的时候，少写了path  
component写成了components
路径写的相对路径，没有用别名的方式
学会了 require.context

