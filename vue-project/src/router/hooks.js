import store from '../store';
import * as types from '../store/action-types'
// 登录权限校验
const loginPermission = async function(to, from, next) {
  //console.log(this)//绑在router上
    let r = await store.dispatch(`user/${types.USER_VALIDATE}`);



      //根据meta来判断是否登录过
      let needLogin = to.matched.some(item => item.meta.needLogin);
      //console.log(needLogin)//ture为需要登录权限，false为不需要登录查看
      if (!store.state.user.hasPermission) {
          // 没登录 但是访问这个页面需要登录
          if (needLogin) {
              if (r) {
                  next(); // 需要登录 并且登录过了 继续即可
              } else {
                  next('/login');//需要登录 且没有登陆就返回登录页进行登录
              }
          } else {
              next(); // 没登录 也不用权限
          }
      } else {
          // 登录过 访问登录页面 跳转到首页 
          if (to.path === '/login') {
              next('/');
          } else {//登录过没有访问登录页面想干什么就干什么
              next();
          }
      }
      next();
}



// 路由权限动态添加
export const menuPermisson = async function(to, from, next) {
    if (store.state.user.hasPermission) {
        // 添加路由这里需要判断是否添加过路由了 
        if (!store.state.user.menuPermission) {
            // 添加路由完成，但是是属于异步加载，不会立即更新
            store.dispatch(`user/${types.ADD_ROUTE}`);
           //replace 相当于replaceState替换掉并不进入历史记录 把路径添加完全
            next({ ...to, replace: true }); // 如果是next()时，进入到页面时报404.
        } else {
            next();
        }
    } else {
        next();
    }
}

export const createWebSockect = async function (to,from,next) {
    if(store.state.user.hasPermission&&!store.state.ws){
        store.dispatch(`${types.CREATE_WEBSOCK}`)
        next()
    }else{
        next()
    }
}

export default {
    loginPermission,
    menuPermisson,
    createWebSockect
}