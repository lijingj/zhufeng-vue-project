export default [{
  path:'/',
  component:()=>import("@/views/Home.vue"),
  beforeEnter(){
    console.log('beforeEnter')
  }
},
{
  path:'*',
  component:()=>import("../views/404.vue")
}
, {
  path:'/manager',
  component:()=> import(/*webpackChunkName:'manager'*/'@/views/manager/index.vue'),
  meta:{
      needLogin:true
  }
}
]