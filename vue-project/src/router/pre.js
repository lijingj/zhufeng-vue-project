export default [{
  path: 'userStatistics',
  meta: {
      auth: 'userStatistics'
  },
  name: 'userStatistics',
  component: () => import( '@/views/manager/userStatistics.vue')
},
{
  path: 'userAuth',
  meta: {
      auth: 'userAuth'
  },
  name: 'userAuth',
  component: () => import( '@/views/manager/userAuth.vue')
},
{
  path: 'infoPublish',
  meta: {
      auth: 'infoPublish'
  },
  name: 'infoPublish',
  component: () => import( '@/views/manager/infoPublish.vue')
},
{
  path: 'articleManager',
  meta: {
      auth: 'articleManager'
  },
  name: 'articleManager',
  component: () => import( '@/views/manager/articleManager.vue')
},
{
  path: 'personal',
  name: 'personal',
  meta: {
      auth: 'personal'
  },
  component: () => import( '@/views/manager/personal.vue')
},
{
  path: 'myCollection',
  meta: {
      auth: 'myCollection'
  },
  name: 'myCollection',
  component: () => import( '@/views/manager/myCollection.vue')
},
{
  path: 'privateMessage',
  meta: {
      auth: 'privateMessage'
  },
  name: 'privateMessage',
  component: () => import( '@/views/manager/privateMessage.vue')
},
{
  path: 'myArticle',
  meta: {
      auth: 'myArticle'
  },
  name: 'myArticle',
  component: () => import( '@/views/manager/myArticle.vue')
}
]