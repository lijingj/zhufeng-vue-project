import Vue from 'vue'
import VueRouter from 'vue-router'
import hooks from './hooks';
import Home from '../views/Home.vue'

Vue.use(VueRouter)

//   const routes = [
//   {
//     path: '/',
//     name: 'Home',
//     component: Home
//   },
//   {
//     path: '/about',
//     name: 'About',
//     // route level code-splitting
//     // this generates a separate chunk (about.[hash].js) for this route
//     // which is lazy-loaded when the route is visited.
//     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
//   }
// ]


//require.context()是webpack语法，创建自己的（模块）上下文。
//三个参数：搜索文件夹目录，是否搜索子目录，匹配文件的正则表达式
const files = require.context('./',false,/\.router.js$/);
console.log(files,files.keys());
const routes = [];
files.keys().forEach(key=>{
  routes.push(...files(key).default);
});
console.log(routes);

// hack router push callback
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
console.log(hooks)
Object.values(hooks).forEach(hook=>{

  router.beforeEach(hook.bind(router)); // 将this绑定成router
  
  // router.beforeEach((to,from,next)=>{
  //   hook.bind(router,to,from,next)
  //   console.log('beforeEach')
  // }); // 将this绑定成router

  // router.beforeResolve((to,from,next)=>{
  //   console.log('resolveEach')
  // });


  // router.afterEach((to,from)=>{
  //   console.log('afterEach')
  // });
});

export default router
