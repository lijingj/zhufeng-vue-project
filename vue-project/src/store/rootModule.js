import { getSlider } from '../api/public';
import WS from '@/utils/websocket'
import * as types from './action-types';//存放所有方法的类型，为了直观看到哪些方法调用

//console.log(types)
export default {
    state: {
        sliders: [],
        ws:null,
        message:''
    },
    mutations: {
        [types.SET_SLIDERS](state, payload) {
            state.sliders = payload;
        },
        [types.CREATE_WEBSOCK](state,ws){
            state.ws = ws
        },
        [types.SET_MESSAGE](state,message){
            state.message = message
        }
      },
    actions: {
      //调用getSlider()的api
        async [types.SET_SLIDERS]({ commit }) {
            let { data } = await getSlider();
            //console.log(data)
            commit(types.SET_SLIDERS, data)
        },
        async [types.CREATE_WEBSOCK]({commit}){
            let ws = new WS()
            ws.create()
            commit(types.CREATE_WEBSOCK,ws)
        }
    }
}