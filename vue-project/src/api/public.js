import config from './config/public';
import axios from '@/utils/request'
import { getLocal } from '@/utils/local.js' //新建local.js文件存储本地会话

// 获取轮播图功能
export const getSlider = () => axios.get(config.getSlider);

// 创建一个唯一的用户标识 和  验证码对应上  1：1234
export const getCaptcha = () => axios.get(config.getCaptcha, {
  //通过获取本地会话可以得到不同的uid的值，也就是获取不同的验证码
    params: {
        uid: getLocal('uuid')
    }
})